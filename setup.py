import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'SQLAlchemy',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'pyramid_chameleon',
    'zope.sqlalchemy',
    'waitress',
    'bulbs',
    'dota2py',
    'gmpy2'
    ]

setup(name='ostigaming_py',
      version='0.0',
      description='ostigaming_py',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='micael bergeron',
      author_email='micaelbergeron@gmail.com',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='ostigaming_py',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = ostigaming_py:main
      [console_scripts]
      initialize_ostigaming_py_db = ostigaming_py.scripts.initializedb:main
      """,
      )

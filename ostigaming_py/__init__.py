import os
from pyramid.config import Configurator
from pyramid.renderers import JSON
from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker

here = os.path.dirname(os.path.abspath(__file__))
DBEngine = None
DBSession = sessionmaker()

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    global DBEngine
    DBEngine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=DBEngine)

    print settings['mako.directories']
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')

    config.add_renderer('prettyjson', JSON(indent=4))
    config.add_static_view('static', 'static', cache_max_age=0)
    config.add_static_view('http://cdnjs.cloudflare.com', 'ostigaming-py:cdnjs')

    config.add_route('home', '/')
    config.add_route('raw_dota', '/raw/dota/{query}')
    config.add_route('dota', '/dota/{query}')

    config.scan()
    return config.make_wsgi_app()

/**
 * Created by micael on 13-09-16.
 */

var HERO_COUNT = 100;

function findMostPlayedHero(matches) {
    var heroPick = {};
    $.each(matches, function(idx, m) {
        $.each(m.players, function(idx, p) {
           if (heroPick[p.hero_id] === undefined)
             heroPick[p.hero_id] = 0;
           heroPick[p.hero_id] += 1;
        });
    });

    var orderedHeroes = Object.keys(heroPick);
    orderedHeroes.sort(function(a,b) {
       return heroPick[b] - heroPick[a];
    });

    return orderedHeroes[0];
}

$(document).ready(function() { });
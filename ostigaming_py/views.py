from dota2py.api import get_match_details, set_api_key
from pyramid.renderers import render_to_response
from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.url import route_url

from scripts.dotahelper import Dotahelper
from scripts.SteamID import SteamID
from sqlalchemy.exc import DBAPIError
from dota2py import api

api.set_api_key('0177E6196067A89545B6D4E31B455BD6')


@view_config(route_name='home', renderer='templates/simple.pt')
def my_view(request):
    return {}
    
@view_config(route_name='raw_dota', renderer='prettyjson')
def dota(request):
    helper = Dotahelper(request.registry.settings)
    helper.test()
    print api

    response = api.get_steam_id(request.matchdict['query'])
    print response

    steam_id = SteamID(response["response"]["steamid"])
    history = api.get_match_history(account_id=steam_id.account_number)

    for match in history['result']['matches'][::-1]:
        players = match.pop('players')
        helper.create_match(match)
        for player in players:
            try:
                helper.create_player(player)
            except KeyError:
                print "Couldn't save player %s" % player
                pass
        helper.create_match_relation(match, players)

    return {'history': history, "steam_id": steam_id}


@view_config(route_name='dota', renderer='templates/dota.pt')
def dota_view(request):
    print request.matchdict['query']
    path = request.route_path('raw_dota', query=request.matchdict['query'])
    return HTTPFound(path)


conn_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_ostigaming_py_db" script
    to initialize your database tables.  Check your virtual 
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""


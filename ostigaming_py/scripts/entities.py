from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from ostigaming_py import DBEngine
from sqlalchemy import Column, Integer, BigInteger, String, Boolean, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import UUID

import uuid
from datetime import datetime

__author__ = 'micael'

class PlayerRoster(Base):
    __tablename__ = 'playerrosters'
    id = Column(UUID(as_uuid=True), primary_key=True)

    player_slot = Column(Integer, nullable=False)
    hero_id = Column(Integer, nullable=False)
    gold_per_minute = Column(Integer)
    xp_per_minute = Column(Integer)
    kill_c = Column(Integer)
    death_c = Column(Integer)
    assist_c = Column(Integer)
    is_victorious = Column(Boolean, default=False)

    player_id = Column(UUID(as_uuid=True), ForeignKey('players.id'))
    match_seq_num = Column(Integer, ForeignKey('matches.match_seq_num'))

    player = relationship('Player', backref=backref('rosters', order_by=match_seq_num))
    match = relationship('Match', backref=backref('rosters', order_by=match_seq_num))

    def __init__(self):
        self.id = uuid.uuid4()

    @property
    def k(self):
        return self.xp_per_minute

    @k.setter
    def k(self, value):
        self.kill_c = value

    @property
    def d(self):
        return self.death_c

    @d.setter
    def d(self, value):
        self.death_c = value


class Match(Base):
    __tablename__ = 'matches'
    id = Column(UUID(as_uuid=True), primary_key=True)
    match_id = Column(Integer, unique=True, index=True)
    match_seq_num = Column(Integer, unique=True)
    start_time = Column(DateTime, nullable=False)
    lobby_type = Column(Integer, nullable=False)

    def __init__(self):
        self.id = uuid.uuid4()


class Player(Base):
    __tablename__ = 'players'
    id = Column(UUID(as_uuid=True), primary_key=True)
    account_id = Column(BigInteger, unique=True, index=True)

    def __init__(self):
        self.id = uuid.uuid4()

Base.metadata.create_all(DBEngine)




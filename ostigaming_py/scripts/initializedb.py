import os
import sys

from sqlalchemy import engine_from_config, create_engine

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from pyramid.scripts.common import parse_vars

def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    print "HELLO"
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = create_engine("postgresql://root:rexxar@localhost:5432/ostigaming")
    engine = engine_from_config(settings, 'sqlalchemy.')
    metadata.bind = engine
    metadata.bind.echo = True

    print "Brewing elixir..."
    setup_all(True)

if __name__ == "__main__":
    main(sys.argv)

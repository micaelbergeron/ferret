import json

__author__ = 'micael'

class SteamID:

    STEAMID64_K = 76561197960265728L
    STEAMID32_ANONYMOUS = 4294967295L

    @property
    def raw(self):
        return self._raw

    def __init__(self):
        self._raw = 0
        self.__init__(SteamID.STEAMID64_K + SteamID.STEAMID32_ANONYMOUS)

    def __init__(self, steamid_32or64):
        self._raw = 0

    @property
    def account_number(self):
        return self._raw - SteamID.STEAMID64_K

    @account_number.setter
    def set_account_number(self, value):
        self._raw = value

    @raw.setter
    def set_steam_id(self, steamid_64):
        self._raw = steamid_64

    def __repr__(self):
        return json.dumps(self.__dict__)
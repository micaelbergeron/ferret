from py2neo import neo4j, node, rel
from entities import *
from ostigaming_py import DBSession


class Dotahelper:

    def __init__(self, settings):
        self.last_mnode = None
        self.sql = DBSession()
        self.g = neo4j.GraphDatabaseService(settings['neo4j.url'])

    def create_match(self, match):
        match_sql = Match.get_by(match_id=int(match['match_id']))
        if match_sql is None:
            match_sql = Match()
            match_sql.from_dict(match)
            match_sql.start_time = datetime.utcfromtimestamp(match['start_time'])

        created_count = [0, 0, 0]    # match/match-[:follows]>match/player
        mapped_count = [0, 0, 0]
        match_index = self.g.get_or_create_index(neo4j.Node, 'match_index')
        player_index = self.g.get_or_create_index(neo4j.Node, 'player_index')

        batch = self.g
        mnode = batch.get_or_create_indexed_node('match_index',
                                                 'match_id',
                                                 match['match_id'],
                                                 match)
        session.commit()

        if self.last_mnode is not None:
            batch.create(rel(mnode, "follows", self.last_mnode))
            created_count[1] += 1
        self.last_mnode = mnode

    def create_match_relation(self, match, players):
        match_node = self.g.get_indexed_node('match_index', 'match_id', match['match_id'])
        match_sql = Match.get_by(match_id=int(match['match_id']))
        if match_node is None:
            raise Exception("Cannot find match %s in current graph." % match)

        if match_sql is None:
            raise Exception("Cannot find match %s in current RDBMS." % match)

        created_count = 0
        for player in players:
            try:
                player_sql = Player.get_by(account_id=int(player['account_id']))
                if player_sql is not None:
                    player_roster_sql = PlayerRoster.get_by(player=player_sql, match=match_sql)
                    if player_roster_sql is None:
                        player_roster_sql = PlayerRoster()
                        player_roster_sql.from_dict(player)
                        player_roster_sql.player = player_sql
                        player_roster_sql.match = match_sql

                relation_data = {
                    "position": player['player_slot'],
                    "hero_id": player['hero_id']
                }
                player_node = self.g.get_indexed_node('player_index', 'account_id', player['account_id'])
                if player_node is not None:
                    self.g.create(rel(player_node, 'played', match_node), relation_data)
                    created_count += 1
            except KeyError:
                print "Couldn't save player-[:played]->match %s." % player
                pass

        print "Created %s player-[:played]->match relation(s)." % created_count
        session.commit()

    def create_player(self, player):
        player_sql = Player.get_by(account_id=int(player['account_id']))
        if player_sql is None:
            player_sql = Player()
            player_sql.from_dict(player)

        self.g.get_or_create_indexed_node('player_index', 'account_id', player['account_id'], player)
        session.commit()

    def test(self):
        print "Test"

var Absurd = require('absurd');
var path = require('path');
var fs = require('fs');

function debug(val, name) {
    return;
    if (name === undefined) return console.log(val);
    return console.log('[' + name + ']: ' + val);
}

var output = path.resolve("ostigaming_py/assets/css");
var input = path.resolve("ostigaming_py/scripts/absurdjs");

var findit = require('findit')(input);

findit.on('directory', function(dir) {
    console.log('dir:' + dir);
    var dir = dir.split(input).join('');

    var output_path = path.join(output, dir);
    console.log('output_path: ' + output_path);
    fs.mkdir(output_path, '0755', function(ex) {
        // i dont care if it exists
        if (ex
        && ex.errno != 47)
            console.log(ex);
    });
});

findit.on('file', function(file) {
    var filter = /.*\.js/i;
    if (!file.match(filter)) {
        console.log('[SKIP]');
        return;
    }

    debug(file, 'file');
    var srcdir = path.dirname(file);
    debug(srcdir, 'dir_src');
    var relativesrc = srcdir.split(input).join('');
    debug(relativesrc, 'rel_src');

    var destfile = path.join(output, relativesrc, path.basename(file, '.js') + '.css');

    console.log(path.join(relativesrc, path.basename(file)) + "\t-[AbsurdJS]>\t" + path.join(relativesrc, path.basename(destfile)));
    Absurd(file).compileFile(destfile, function(err, css) {
        if (err)
            console.log(err);
    });
});

module.exports = function(grunt) {

}